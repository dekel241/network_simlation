/*
 * node.c
 *
 *  Created on: Jan 9, 2019
 *      Author: Dekel
 */

#include "node.h"
#include "graph.h"
#include "protocol.h"
#include "config.h"
#include "cmd.h"
#include "util.h"
#include "network.h"

#include <stdio.h>
#ifdef _WIN32
#include <winsock.h>
#include <io.h>
#else //Linux
	#include <netinet/ip.h>
	#include <netinet/in.h>
	#include <unistd.h>
	#include <arpa/inet.h>
#endif //_WIN32
#include <assert.h>
#include <stdlib.h>
#include <memory.h>

static void handle_message(node_t * node, int connfd, int (*rt)[AMOUNT_OF_NODES]);
static void init_routing_table(int (*rt)[AMOUNT_OF_NODES], int curr_node);

static graph_t g_graph;//For mapping neighbors between nodes.

int node_global_init(void)
{
	int retval = 0;

	if(graph_init(&g_graph, AMOUNT_OF_NODES) < 0)
	{
		printf("graph_init() failed\n");
		retval = -1;
	}

	return retval;
}

void node_global_destroy(void)
{
	graph_destroy(&g_graph);
}

void node_print_neighbors(int node)
{
	graph_print_neighbors(&g_graph, node);
}

bool node_is_neighbors(int node_i, int node_j)
{
	return graph_linked(&g_graph, node_i, node_j);
}

void * node_thread(void * args)
{
	assert(args != NULL);

	//Convert into node data.
	node_t * node = (node_t *)args;
	struct sockaddr_in servaddr, src_node;
	int routing_table[AMOUNT_OF_NODES][AMOUNT_OF_NODES];
	init_routing_table(routing_table, node->id);
	 // assign IP, PORT
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(network_get_port(node->id));

	 // Binding newly created socket to given IP and port.
	if ((bind(node->sock, (struct sockaddr*)&servaddr, sizeof(servaddr))) != 0)
	{
		printf("socket bind failed. Node ID: %d\n", node->id);
		exit(0);
	}

	// Now server is ready to listen
	if ((listen(node->sock, AMOUNT_OF_NODES)) != 0) {
		printf("Listen() failed. Node ID: %d\n", node->id);
		exit(0);
	}

	int len = sizeof(src_node);
	while(true)
	{
		// Accept the data packet from client.
		int connfd = accept(node->sock, (struct sockaddr *)&src_node, &len);
		if (connfd < 0)
		{
			printf("server accept() failed. Node ID: %d\n", node->id);
			exit(0);
		}

		handle_message(node, connfd, routing_table);
	}
}

static void handle_message(node_t * node, int connfd, int (*rt)[AMOUNT_OF_NODES])
{
	/*
	 * After sending respond the socket will be closed unless the message was "send"
	 * and that means the session may contain more then one message(so we keep the socket open).
	 */
	bool close_connection = true;
	char buff[HEADER_SIZE + PAYLOAD_SIZE];
	int open_sock = -1;//Socket to mark open connection where the node acts as a client.

	do
	{
		int size = recv(connfd, buff, sizeof(buff), 0);
		if(size <= 0)
		{
			return;
		}

		message_t msg;
		func_id_e func = ACK;
		memcpy(&msg, buff, sizeof(msg));
		//Relevant for all messages.
		int dst_node = atoi((char *)msg.payload);
		char respond[HEADER_SIZE + PAYLOAD_SIZE];

		if(!(util_validate_node_id(msg.src_id)))
		{
			printf("invalid node id:%d\n", msg.src_id);
			return;
		}

		if(msg.func_id == SETID)
		{
			//Validate destination is the receiving node
			if(msg.dst_id != node->id || dst_node != node->id)
			{
				printf("Message designate to node %d but received at node %d...ignoring\n", msg.dst_id, node->id);
				func = NACK;
			}

			message_t msg_answer = {.msg_id = protocol_get_msg_id(), .src_id = msg.dst_id,
					.dst_id = msg.src_id, .trailing = 0, .func_id = func};
			memcpy(msg_answer.payload, &msg.msg_id, sizeof(msg.msg_id));
			protocol_create_message(respond, HEADER_SIZE + PAYLOAD_SIZE , &msg_answer);
			//After sending ack, CLI context will be set by CMD module.
			size = send(connfd, respond, sizeof(respond), 0);
			if(size <= 0)
			{
				printf("send() failed. From Node ID: %d\n", node->id);
			}
		}
		else if(msg.func_id == CONNECT)//Connect message
		{
			bool connect = true;
			if(msg.dst_id != node->id)
			{
				func = NACK;
				connect = false;
			}

			message_t msg_answer = {.msg_id = protocol_get_msg_id(), .src_id = node->id,
					.dst_id = msg.src_id, .trailing = 0, .func_id = func};
			char node_str[5];//Assuming maximum node id is 9999
			itoa(node->id, node_str, 10);
			memcpy(msg_answer.payload, node_str, sizeof(node_str));
			protocol_create_message(respond, sizeof(respond), &msg_answer);

			size = send(connfd, respond, sizeof(respond), 0);
			if(size <= 0)
			{
				printf("send() failed. From Node ID: %d\n", node->id);
				connect = false;
			}

			//Link the two nodes together.
			if(connect)
			{
				graph_link(&g_graph, node->id, msg.src_id);
				//Update routing table.
				rt[msg.src_id][0] = 2;
				rt[msg.src_id][1] = node->id;
				rt[msg.src_id][1] = msg.src_id;
			}
		}
		else if(msg.func_id == SEND)
		{
			int src_id = msg.src_id;
			size_t msg_size = atoi((char *)msg.payload);//Convert into number

			/*
			 * Validate destination is the receiving node.
			 * if we have already pending connection then we should not respond with
			 *  NACK(close_connection true means we have no pending connection).
			 */
			if(msg.dst_id == node->id)
			{
				char data[MAX_MSG_SIZE];//The actual message.
				memset(data, 0, sizeof(data));
				memcpy(data, msg.payload + sizeof(msg_size), MAX_PAYLOAD_SIZE);
				int data_ind = MAX_PAYLOAD_SIZE;//Index to fill the data buffer.

				//The first message contains maximum of MAX_PAYLOAD_SIZE bytes.
				msg_size = msg_size <= (MAX_PAYLOAD_SIZE) ? 0 : msg_size - (MAX_PAYLOAD_SIZE);
				//We want to print the data before the node gets ACK respond
				if(msg_size == 0)
				{
					//Means we gathered all message, we can now print it.
					printf("%s\n", data);
				}
				message_t msg_answer = {.msg_id = protocol_get_msg_id(), .src_id = node->id,
						.dst_id = msg.src_id, .trailing = 0, .func_id = func};
				snprintf((char*)msg_answer.payload, sizeof(int), "%d", msg.msg_id);
				protocol_create_message(respond, sizeof(respond), &msg_answer);

				size = send(connfd, respond, sizeof(respond), 0);
				if(size <= 0)
				{
					printf("send respond failed. From Node ID: %d\n", node->id);
				}

				while(msg.trailing > 0 && msg_size > 0)
				{
					func = ACK;
					size = recv(connfd, buff, sizeof(buff), 0);
					memcpy(&msg, buff, sizeof(msg));

					if(msg.func_id != SEND || msg.dst_id != node->id || msg.src_id != src_id)
					{
						printf("Send data corruption. src_id: %d, func_id: %d, dst_id: %d\n", msg.src_id, msg.func_id, msg.dst_id);
						func = NACK;
					}

					size_t cpy_size =  msg_size < PAYLOAD_SIZE ? msg_size : PAYLOAD_SIZE;
					memcpy(data + data_ind, msg.payload , cpy_size);
					data_ind += PAYLOAD_SIZE;
					msg_size = msg_size <= PAYLOAD_SIZE ? 0 : msg_size - PAYLOAD_SIZE;//except the first message, all messages may have size of PAYLOAD_SIZE.
					if(msg_size == 0)
					{
						//Means we gathered all message, we can now print it.
						printf("%s\n", data);
					}
					msg_answer.msg_id = protocol_get_msg_id();
					msg_answer.func_id = func;
					snprintf((char*)msg_answer.payload, sizeof(int), "%d", msg.msg_id);
					protocol_create_message(respond, sizeof(respond), &msg_answer);
					size = send(connfd, respond, sizeof(respond), 0);
					if(size <= 0)
					{
						printf("send respond failed. From Node ID: %d\n", node->id);
					}
				}

				if(msg.trailing > 0 || msg_size > PAYLOAD_SIZE)
				{
					printf("Number of message left: %d\nMessage size left: %u\n", msg.trailing, msg_size);
				}

				//Only after receiving send message, we can be surly to close the connection after it.
				close_connection = true;
			}
			else if(!close_connection)//handling state when need to relay the send message(means the connection is alive)
			{
				//Relay the "SEND" message
				protocol_create_message(buff, sizeof(buff), &msg);
				if(open_sock == -1)//Means we didn't relay messages(even not a RELAY message) till now, so it's time to.
				{
					int sock;
					int size = netowrk_send_to_node(msg.dst_id, buff, sizeof(buff), &sock);
					if(size <= 0)
					{
						printf("send() failed\n");
						return;
					}
					//Save the new socket connection.
					open_sock = sock;
				}
				else
				{
					if(send(open_sock, buff, sizeof(buff), 0) <= 0)
					{
						printf("send() failed\n");
						return;
					}//Wait for respond
				}

				if(recv(open_sock, respond, sizeof(buff), 0) <= 0)
				{
					printf("recv() failed\n");
					return;
				}//Return the answer message to the initiator.
				else if(send(connfd, respond, sizeof(buff), 0) <= 0)
				{
					printf("send() to initiator failed\n");
					return;
				}

				if(msg.trailing == 0)
				{
					close_connection = true;
				}
			}
		}
		else if(msg.func_id == DISCOVER)
		{
			//Create route message
			message_t msg_answer = {.msg_id = protocol_get_msg_id(), .src_id = node->id,
					.dst_id = msg.src_id, .trailing = 0, .func_id = ROUTE};
			//Validate destination is the receiving node
			if(msg.dst_id != node->id)
			{
				printf("Message designate to node %d but received at node %d...ignoring\n", msg.dst_id, node->id);
				msg_answer.func_id = NACK;
			}
			else
			{
				//First, check if the destination node is connected to the receiving node.
				if(node_is_neighbors(node->id, dst_node))
				{
					snprintf((char*)msg_answer.payload, sizeof(msg.msg_id), "%d", msg.msg_id);
					snprintf((char*)(msg_answer.payload + sizeof(msg.msg_id)), sizeof(int), "%d", 2);//Distance of two nodes
					snprintf((char*)(msg_answer.payload + sizeof(msg.msg_id)*2), sizeof(int), "%d", node->id);
					snprintf((char*)(msg_answer.payload + sizeof(msg.msg_id)*3), sizeof(int), "%d", dst_node);
				}
				else if(rt[dst_node][0] > 0)//If we already have route for the node.
				{
					snprintf((char*)msg_answer.payload, sizeof(msg.msg_id), "%d", msg.msg_id);
					snprintf((char*)(msg_answer.payload + sizeof(msg.msg_id)), sizeof(int), "%d", rt[dst_node][0]);
					for(int i = 1; i < rt[dst_node][0] + 1; i++)
					{
						//Copy to the payload the full path.
						snprintf((char *)(msg_answer.payload + sizeof(dst_node)*(i + 1)),sizeof(int), "%d", rt[dst_node][i]);
					}
				}
				else//We have to send discover to all neighbors
				{
					int min_path;
					int routes[AMOUNT_OF_NODES];
					if(!cmd_send_dicover_message(msg.src_id, node->id, dst_node, &open_sock, &min_path, routes))
					{
						//Means we have no route for the destination. Returns Nack.
						msg_answer.func_id = NACK;
					}
					else
					{
						snprintf((char*)msg_answer.payload, sizeof(msg.msg_id), "%d", msg.msg_id);
						snprintf((char*)(msg_answer.payload + sizeof(msg.msg_id)), sizeof(int), "%d", routes[0]);
						rt[dst_node][0] = routes[0];//Copy record into routing table.
						for(int i = 1; i < routes[0] + 1; i++)
						{
							//Copy to the payload the full path.
							snprintf((char *)(msg_answer.payload + sizeof(dst_node)*(i + 1)),sizeof(int), "%d", routes[i]);
							//Update routing table.
							rt[dst_node][i] = routes[i];
						}
					}
				}
			}

			//Send answer to the initiator.
			protocol_create_message(respond, sizeof(respond), &msg_answer);
			size = send(connfd, respond, sizeof(respond), 0);
			if(size <= 0)
			{
				printf("send respond failed. From Node ID: %d\n", node->id);
			}
		}
		else if(msg.func_id == RELAY)
		{
			//Handle first relay - the relay to the current node
			int src_id = msg.src_id;
			//Validate destination is the receiving node
			if(msg.dst_id != node->id)
			{
				printf("Message designate to node %d but received at node %d...ignoring\n", msg.dst_id, node->id);
				func = NACK;
			}

			message_t msg_answer = {.msg_id = protocol_get_msg_id(), .src_id = node->id,
					.dst_id = msg.src_id, .trailing = 0, .func_id = func};

			int amount_relay = atoi((char *)(msg.payload + sizeof(int)));//Get number of nodes to the destination.

			snprintf((char*)msg_answer.payload, sizeof(int), "%d", msg.msg_id);
			protocol_create_message(respond, sizeof(respond), &msg_answer);
			size = send(connfd, respond, sizeof(respond), 0);
			if(size <= 0)
			{
				printf("send respond failed. From Node ID: %d\n", node->id);
				return;
			}

			//Handle the rest which are not to the current node.
			for(int i = 0; i < amount_relay - 1; i++)//minus one since we have already got the first one.
			{
				//Receive from the initiator node.
				size = recv(connfd, buff, sizeof(buff), 0);
				if(size <= 0)
				{
					printf("Send Relay message failed\n");
					return;
				}

				//Copy the incoming message from buff.
				memcpy(&msg, buff, sizeof(msg));
				//Validate fields of the data
				if(msg.src_id != src_id || msg.func_id != RELAY)
				{
					printf("Relay data corruption. src_id: %d, func_id: %d\n", msg.src_id, msg.func_id);
					return;
				}

				int next_amount_relay = atoi((char *)(msg.payload + sizeof(int)));//Get number of nodes of next node to the destination.
				if(amount_relay -i -1 != next_amount_relay)
				{
					printf("Data corruption. amount_relay: %d, should be: %d\n", next_amount_relay, amount_relay -1);
					return;
				}

				int size;
				//Send the next relay to the next node in the chain.
				if(open_sock == -1)
				{
					//Save the open socket to the next iterations.
					size = netowrk_send_to_node(msg.dst_id, buff, sizeof(buff), &open_sock);
				}
				else
				{
					size = network_send_open_connection(open_sock, buff, sizeof(buff));
				}
				if(size <= 0)
				{
					printf("send()/send_open() failed\n");
					return;
				}
				if(network_recv_from_node(open_sock, respond, sizeof(respond)) <= 0)
				{
					printf("recv_from_node() failed\n");
					return;
				}

				message_t msg_respond;
				memcpy(&msg_respond, respond, sizeof(msg_respond));

				if(msg_respond.func_id != ACK)
				{
					//func_id of the message designate to the initiator node.
					func = NACK;
				}

				//Now we can send Acknowledge to the initiator node
				msg_answer.msg_id = protocol_get_msg_id();
				msg_answer.func_id = func;

				snprintf((char*)msg_answer.payload, sizeof(int), "%d", msg.msg_id);
				protocol_create_message(respond, sizeof(respond), &msg_answer);
				size = send(connfd, respond, sizeof(respond), 0);
				if(size <= 0)
				{
					printf("send respond failed. From Node ID: %d\n", node->id);
					return;
				}
			}
			//we are now preparing to relay a "SEND" message.
			//Set close connection to false since we are waiting now for SEND message.
			close_connection = false;
		}
		else if(msg.func_id == ROUTE)//If we got a route message, it means we have to send a discover
		{
			 if(msg.dst_id != node->id)
			 {
				 printf("Message designate to node %d but received at node %d...\n", msg.dst_id, node->id);
				 func = NACK;
			 }
			message_t msg_answer = {.msg_id = protocol_get_msg_id(), .src_id = node->id,
						.dst_id = dst_node, .trailing = 0, .func_id = func};

			int sock;
			int min_path;
			int routes[AMOUNT_OF_NODES];
			if(!cmd_send_dicover_message(msg.src_id, node->id, dst_node, &sock, &min_path, routes))
			{
				//Means we have no route for the destination. Returns Nack.
				msg_answer.func_id = NACK;
			}
			else
			{
				snprintf((char*)msg_answer.payload, sizeof(msg.msg_id), "%d", msg.msg_id);//Copy message id
				memcpy(msg_answer.payload + sizeof(msg.msg_id), routes, sizeof(routes));//Copy route to destination
				//Update routing table.
				rt[dst_node][0] = routes[0] + 1;//Copy routes size +1.
				rt[dst_node][1] = node->id;//Copy the current node.
				memcpy(&rt[dst_node][2], &routes[1], sizeof(routes[1]) * routes[0]);//copy the rest of the path.
			}

			//Send answer to the initiator.
			protocol_create_message(respond, sizeof(respond), &msg_answer);
			size = send(connfd, respond, sizeof(respond), 0);
			if(size <= 0)
			{
				printf("send respond failed. From Node ID: %d\n", node->id);
			}

			//Close the connection.
			network_close_connection_node(sock);
		}
		else if(msg.func_id == PEERS)
		{
			//Validate destination is the receiving node
			if(msg.dst_id != node->id)
			{
				func = NACK;
			}

			message_t msg_answer = {.msg_id = protocol_get_msg_id(), .src_id = msg.dst_id,
					.dst_id = msg.src_id, .trailing = 0, .func_id = func};
			memcpy(msg_answer.payload, &msg.msg_id, sizeof(msg.msg_id));
			protocol_create_message(respond, HEADER_SIZE + PAYLOAD_SIZE , &msg_answer);
			//After sending ack, CLI context will be set by CMD module.
			if(send(connfd, respond, sizeof(respond), 0) <= 0)
			{
				printf("send() failed. From Node ID: %d\n", node->id);
			}
		}
		else
		{
			printf("Unrecognized function ID: %d\n", msg.func_id);
		}

	}while(!close_connection);

	close(connfd);
	if(open_sock != -1)
	{
		close(open_sock);
	}
}

static void init_routing_table(int (*rt)[AMOUNT_OF_NODES], int curr_node)
{
	for(int i = 0; i < AMOUNT_OF_NODES; i++)
	{
		if(node_is_neighbors(curr_node, i))
		{
			rt[i][0] = 2;//1 node to the destination
			rt[i][1] = curr_node;
			rt[i][2] = i;
		}
		else if(curr_node == i)
		{
			rt[i][0] = 1;//No route for destination.
			rt[i][1] = curr_node;
		}
		else
		{
			rt[i][0] = 0;//No route for destination.
		}
	}
}
