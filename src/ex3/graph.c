/*
 * graph.c
 *
 *  Created on: Jan 9, 2019
 *      Author: Dekel
 */

#include "graph.h"
#include <stdlib.h>
#include <stdio.h>

int graph_init(graph_t * graph, int size)
{
	graph->size = size;
	//First, allocate the mat
	graph->entry = (bool **)malloc(sizeof(bool *) * size);
	if(graph->entry == NULL)
	{
		return -1;
	}

	for(int i = 0; i < size; i++)
	{
		graph->entry[i] = (bool *)malloc(sizeof(bool) * size);
		if(graph->entry[i] == NULL)
		{
			return -1;
		}
	}

	//After success allocating, initialize the graph.
	for(int i = 0; i < size; i++)
	{
		for(int j = 0; j < size; j++)
		{
			graph->entry[i][j] = false;
		}
	}

	return 0;
}

void graph_destroy(graph_t * graph)
{
	for(int i = 0; i < graph->size; i++)
	{
		free(graph->entry[i]);
	}

	free(graph->entry);
}

void graph_link(graph_t * graph, int node_i, int node_j)
{
	//Since this isn't a directed graph, we have to link both directions.
	graph->entry[node_i][node_j] = true;
	graph->entry[node_j][node_i] = true;
}

void graph_unlink(graph_t * graph, int node_i, int node_j)
{
	graph->entry[node_i][node_j] = false;
	graph->entry[node_j][node_i] = false;
}

bool graph_linked(graph_t * graph, int node_i, int node_j)
{
	return graph->entry[node_i][node_j];
}

void graph_print_neighbors(graph_t * graph, int node)
{
	bool first_iteration = true;
	for(int i = 0; i < graph->size; i++)
	{
		if(graph->entry[node][i] == true)
		{
			if(first_iteration)
			{
				printf("%d", i);
				first_iteration = false;
			}
			else
				printf(",%d", i);
		}
	}
	if(!first_iteration)
	{
		printf("\n");
	}
}
