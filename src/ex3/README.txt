Notes:
The project was developed for and under MINGW platform(and also fitting CYGWIN)
The Code is Unix/Linux Portable but wasn't checked for these platforms and might
not work as expected.

Description:
In the project i used pthread API in order to assign each node a different thread.
Instead of using a different networks for each node(which is hard to demonstrate)
i used one IP address(Local address - 127.0.0.1) and different ports.

Usage:
In order to use a user defined ports:
1.Open network.c source file.
2.set "g_use_base_port" variable to false.
3.Go to function "network_init()" and modify the ports accordingly.
4.Recompile the code.

*In order to use default ports(1230,1231,...) just set "g_use_base_port" to true.

Compile:
-O0 -g3 -Wall -c -fmessage-length=0 (Standart)

Link:
-pthread
-ws2_32(Since its was compiled under MINGW platform)
