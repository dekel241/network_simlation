/*
 * network.h
 *
 *  Created on: Jan 15, 2019
 *      Author: Dekel
 */

#ifndef EX3_NETWORK_H_
#define EX3_NETWORK_H_

#include <stddef.h>

int network_init(void);
int network_connect_to_node(int node_id);
int netowrk_send_to_node(int node_id, const char * buffer, size_t size, int * output_sock);
int network_send_open_connection(int sock, const char * buffer, size_t size);
int network_recv_from_node(int sock, char recv_buff[], int size);
int network_close_connection_node(int sock);

void netowrk_set_timeout(long time);
long network_get_timeout(void);
unsigned short network_get_port(int node_id);
int network_get_node_id(unsigned short port);

#endif /* EX3_NETWORK_H_ */
