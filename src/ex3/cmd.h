/*
 * cmd.h
 *
 *  Created on: Jan 6, 2019
 *      Author: Dekel
 */

#ifndef EX3_CMD_H_
#define EX3_CMD_H_

#include "config.h"
#include <stdbool.h>

/*
 * The function handles a command came from the CLI.
 */
void cmd_handle(char * cmd);

/*
 * The function return true if there is a path to dst_node from the
 * context node. sock, minimum_path and routes are optional output parameters.
 */
bool cmd_send_dicover_message(int sender, int src_node, int dst_node, int * connfd, int * minimum_path, int * path);

#endif /* EX3_CMD_H_ */
