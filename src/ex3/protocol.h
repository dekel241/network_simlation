/*
 * protocol.h
 *
 *  Created on: Jan 6, 2019
 *      Author: Dekel
 */

#ifndef EX3_PROTOCOL_H_
#define EX3_PROTOCOL_H_

#include <stdint.h>
#include <stddef.h>
#include "node.h"

#define MAX_RESPOND_SIZE 5// + 1 for null terminator.
#define MSG_ACK "ack"
#define MSG_NACK "nack"

#define INET_ADDRSTRLEN 16
#define HEADER_SIZE 20
#define PAYLOAD_SIZE 492

//Maximum message size when the payload contains the message length.
#define MAX_PAYLOAD_SIZE PAYLOAD_SIZE - sizeof(size_t)

typedef enum
{
	SETID = 0,
	ACK = 1,
	NACK = 2,
	CONNECT = 4,
	DISCOVER = 8,
	ROUTE = 16,
	SEND = 32,
	RELAY = 64,
	PEERS = 128
}func_id_e;

typedef struct
{
	uint32_t msg_id;
	int32_t src_id;
	int32_t dst_id;
	uint32_t trailing;
	func_id_e func_id;

	uint8_t payload[PAYLOAD_SIZE];
}__attribute__((packed))message_t;

/*
 * This function should be called once.
 */
int protocol_global_init(void);
void protocol_global_destroy(void);

/*
 * The function creates a message buffer using a message_t struct.
 */
void protocol_create_message(char * buff, size_t size, message_t * msg);

//Getters
node_t * protocol_get_node(int node_id);
int protocol_get_msg_id(void);

#endif /* EX3_PROTOCOL_H_ */
