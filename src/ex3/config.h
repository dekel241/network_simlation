/*
 * config.h
 *
 *  Created on: Jan 11, 2019
 *      Author: Dekel
 */
/*
 * Module for configurations and defines.
 */
#ifndef EX3_CONFIG_H_
#define EX3_CONFIG_H_


#define BASE_PORT 1230//Each node will bind to BASE_PORT + node_id
/*
 * Since it is difficult to demonstrate a network(for checking my code)
 * I'm using one IP address and different port for each node.
 */
#define IP_ADDRESS "127.0.0.1"

#define NO_CONTEXT 65536//No node
#define AMOUNT_OF_NODES 8
#define MAX_MSG_SIZE 2048//Maximum message size can be sent by a user.
#define TIMEOUT 2000 * AMOUNT_OF_NODES//Receive/send message timeout

#endif /* EX3_CONFIG_H_ */
