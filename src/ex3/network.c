/*
 * network.c
 *
 *  Created on: Jan 15, 2019
 *      Author: Dekel
 */
#include "network.h"
#include "config.h"
#include "util.h"

#ifdef _WIN32
	#include <winsock.h>
	#include <io.h>
#else //Linux
	#include <netinet/ip.h>
	#include <netinet/in.h>
	#include <unistd.h>
	#include <arpa/inet.h>
#endif //_WIN32
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <stdbool.h>
#include <assert.h>

static struct sockaddr_in g_server;
static struct timeval g_timeout = {.tv_sec = TIMEOUT, .tv_usec = 0};
static unsigned short g_iptables[AMOUNT_OF_NODES];
static bool g_use_base_port = true;//When defined true, uses BASE_PORT + node_id

int network_init(void)
{
	// assign IP(port will be assigned each time according to the node)
	g_server.sin_family = AF_INET;
	g_server.sin_addr.s_addr = inet_addr(IP_ADDRESS);//Convert into network including big endian(instead of inet_pton)

	if(!g_use_base_port)//For user ports define.
	{
		g_iptables[0] = 1245;
		g_iptables[1] = 1256;
		g_iptables[2] = 1267;
		g_iptables[3] = 1275;
		g_iptables[4] = 1285;
		g_iptables[5] = 1295;
		g_iptables[6] = 1305;
		g_iptables[7] = 1315;
	}
	else
	{
		for(int i = 0; i < AMOUNT_OF_NODES; i++)
		{
			g_iptables[i] = BASE_PORT + i;
		}
	}
	return 0;
}

int network_connect_to_node(int node_id)
{
	int sock;
	struct sockaddr_in servaddr;

	// socket create
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1)
	{
		printf("socket creation failed...\n");
		exit(0);
	}

	if(setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char *)&g_timeout, sizeof(g_timeout)) < 0)
	{
		printf("setsockopt() SO_RCVTIMEO failed...\n");
		exit(0);
	}
	if(setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (const char *)&g_timeout, sizeof(g_timeout)) < 0)
	{
		printf("setsockopt() SO_SNDTIMEO failed...\n");
		exit(0);
	}
	memcpy(&servaddr, &g_server, sizeof(servaddr));
	//edit to the correct port
	servaddr.sin_port =  htons(g_iptables[node_id]);//Convert port into little/big endian

	// connect the client socket to the server socket
	if (connect(sock, (struct sockaddr*)&servaddr, sizeof(servaddr)) != 0)
	{
		printf("connection to node %d failed.\n", node_id);
		return -1;
	}

	return sock;
}

/*
 * Initialize connection to a node and then send a message via the open connection
 * to the node.
 * output_sock - output parameter.
 */
int netowrk_send_to_node(int node_id, const char * buffer, size_t size, int * output_sock)
{
	int sock = network_connect_to_node(node_id);
	if(sock < 0)
	{
		printf("connect_to_node() failed.");
		close(sock);//Close the socket.
		return 0;
		//exit(0);
	}

	*output_sock = sock;
	return send(sock, buffer, size, 0);
}

/*
 * Send a message to node via an open socket. returns number of bytes successfully sent.
 */
int network_send_open_connection(int sock, const char * buffer, size_t size)
{
	return send(sock, buffer, size, 0);
}

int network_recv_from_node(int sock, char recv_buff[], int size)
{
	return recv(sock, recv_buff, size, 0);
}

int network_close_connection_node(int sock)
{
	return close(sock);
}

void netowrk_set_timeout(long time)
{
	if(time > 0)
	{
		g_timeout.tv_sec = time;
	}
}

long network_get_timeout(void)
{
	return g_timeout.tv_sec;
}

unsigned short network_get_port(int node_id)
{
	assert(util_validate_node_id(node_id));

	return g_iptables[node_id];
}

int network_get_node_id(unsigned short port)
{
	for(int i = 0; i < AMOUNT_OF_NODES; i++)
	{
		if(port == g_iptables[i])
		{
			return i;
		}
	}

	return -1;
}
