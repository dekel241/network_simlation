/*
 * util.c
 *
 *  Created on: Jan 7, 2019
 *      Author: Dekel
 */
/*
 * Definitions:
 * None node - the initialization number of g_context(see CLI module).
 */
#include "config.h"
#include "util.h"

bool util_validate_node_id(int node_id)
{
	//Amount of nodes inside the check since it's the number of "None node".
	return ((node_id >= 0 && node_id < AMOUNT_OF_NODES) || (node_id == NO_CONTEXT));
}

#ifndef _WIN32
#include <stdlib.h>

// inline function to swap two numbers
static inline void swap(char *x, char *y) {
	char t = *x; *x = *y; *y = t;
}

// function to reverse buffer[i..j]
static char* reverse(char *buffer, int i, int j)
{
	while (i < j)
		swap(&buffer[i++], &buffer[j--]);

	return buffer;
}

// Iterative function to implement itoa() function in C
char* itoa(int value, char* buffer, int base)
{
	// invalid input
	if (base < 2 || base > 32)
		return buffer;

	// consider absolute value of number
	int n = abs(value);

	int i = 0;
	while (n)
	{
		int r = n % base;

		if (r >= 10)
			buffer[i++] = 65 + (r - 10);
		else
			buffer[i++] = 48 + r;

		n = n / base;
	}

	// if number is 0
	if (i == 0)
		buffer[i++] = '0';

	// If base is 10 and value is negative, the resulting string
	// is preceded with a minus sign (-)
	// With any other base, value is always considered unsigned
	if (value < 0 && base == 10)
		buffer[i++] = '-';

	buffer[i] = '\0'; // null terminate string

	// reverse the string and return it
	return reverse(buffer, 0, i - 1);
}

char *strlwr(char *str)
{
  unsigned char *p = (unsigned char *)str;

  while (*p) {
     *p = tolower((unsigned char)*p);
      p++;
  }

  return str;
}

#endif //_WIN32
