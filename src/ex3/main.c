/*
 * main.c
 *
 *  Created on: Jan 6, 2019
 *      Author: Dekel
 */
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#ifdef _WIN32
#include <winsock.h>
#endif//_WIN32
#include "node.h"
#include "cli.h"
#include "protocol.h"
#include "network.h"
#include "config.h"

int main(void)
{

#ifdef _WIN32//Both 32 bit and 64 bit
	WSADATA wsa;
	//Since i'm programming on Windows OS, even CYGWIN and MINGW asks for WSAStartup before.
	if(WSAStartup(MAKEWORD(2,2),&wsa) != 0)
	{
		printf("Failed. Error Code : %d",WSAGetLastError());
		exit(-1);
	}
#endif//_WIN32
	if(network_init() != 0)
	{
		printf("network_init() failed\n");
		exit(-1);
	}
	if(protocol_global_init() != 0)
	{
		printf("protocol_global_init() failed\n");
		exit(-1);
	}
	if(node_global_init() != 0)
	{
		printf("node_global_init() failed\n");
		exit(-1);
	}

	pthread_t nodes[AMOUNT_OF_NODES];//Thread for each node.

	//Create the threads
	for(int i = 0; i < AMOUNT_OF_NODES; i++)
	{
		pthread_create(&nodes[i], NULL, node_thread, protocol_get_node(i));
	}

	cli_begin();

	//Wait for nodes thread to finish.
	for(int i = 0; i < AMOUNT_OF_NODES; i++)
	{
		pthread_join(nodes[i], NULL);
	}

	node_global_destroy();
	protocol_global_destroy();

	return EXIT_SUCCESS;
}


