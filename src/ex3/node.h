/*
 * node.h
 *
 *  Created on: Jan 6, 2019
 *      Author: Dekel
 */

#ifndef EX3_NODE_H_
#define EX3_NODE_H_
#include <stdbool.h>

typedef struct
{
	int id;
	int sock;
}node_t;

int node_global_init(void);
void node_global_destroy(void);
void node_print_neighbors(int node);
bool node_is_neighbors(int node_i, int node_j);
void * node_thread(void * args);

#endif /* EX3_NODE_H_ */
