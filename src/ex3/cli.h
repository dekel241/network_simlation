/*
 * cli.h
 *
 *  Created on: Jan 7, 2019
 *      Author: Dekel
 */

#ifndef EX3_CLI_H_
#define EX3_CLI_H_

void cli_begin();

void cli_set_context(int node_id);
int cli_get_context(void);

#endif /* EX3_CLI_H_ */
