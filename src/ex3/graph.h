/*
 * graph.h
 *
 *  Created on: Jan 9, 2019
 *      Author: Dekel
 */

/*
 * This is an implementation of undirected graph.
 */
#ifndef EX3_GRAPH_H_
#define EX3_GRAPH_H_

#include <stdbool.h>
#include <stddef.h>

typedef struct
{
	bool ** entry;//Entry of the graph(entry to the mat)
	size_t size;//Size of the graph - n X n
}graph_t;

int graph_init(graph_t * graph, int size);
void graph_destroy(graph_t * graph);

void graph_link(graph_t * graph, int node_i, int node_j);
void graph_unlink(graph_t * graph, int node_i, int node_j);

bool graph_linked(graph_t * graph, int node_i, int node_j);
void graph_print_neighbors(graph_t * graph, int node);

#endif /* EX3_GRAPH_H_ */
