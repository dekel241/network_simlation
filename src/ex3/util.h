/*
 * util.h
 *
 *  Created on: Jan 7, 2019
 *      Author: Dekel
 */

#ifndef EX3_UTIL_H_
#define EX3_UTIL_H_

#include <stdbool.h>

/*
 * Returns true if node_id is valid.
 */
bool util_validate_node_id(int node_id);

#ifndef _WIN32
/*
 * Standard itoa
 */
char* itoa(int value, char* buffer, int base);
char *strlwr(char *str);
#endif //_WIN32
#endif /* EX3_UTIL_H_ */
