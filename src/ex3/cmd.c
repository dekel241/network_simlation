/*
 * cmd.c
 *
 *  Created on: Jan 7, 2019
 *      Author: Dekel
 */

#include "cmd.h"
#include "cli.h"
#include "util.h"
#include "protocol.h"
#include "network.h"
#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef int (*command)(char * cmd);
#define CMD_TYPE_MAX_SIZE 30

//Commands
static const char * g_setid = "setid";
static const char * g_connect = "connect";
static const char * g_send = "send";
static const char * g_route = "route";
static const char * g_peers = "peers";

/*
 * Sets node id.
 * Format:
 * setid,<id>
 */
static int set_id(char * cmd);

/*
 * Returns 0 on success otherwise -1.
 * Format:
 * connect,<ip>:<port>
 */
static int connect(char * cmd);

/*
 * Returns the number of bytes sent.
 * Format:
 * send,<id>,<len>,<message>
 */
static int send(char * cmd);

/*
 * Prints the route to a specific node.
 * Format:
 * route,<id>
 */
static int route(char * cmd);

/*
 * Prints all connected nodes to the current node
 * Format:
 * peers
 */
static int peers(char * cmd);


/*
 * The function parses a command and returns his tokens inside params.
 */
static int cmd_parse(char * cmd , char params[][MAX_MSG_SIZE], int amount);
static int send_discover(int dst_node, char * message, size_t msg_size);
static int relay(int routes[AMOUNT_OF_NODES], uint32_t num_of_relays, int dst_node, int * connfd);
static int cmd_send_inner(int dst_node, size_t msg_size, char * payload, int open_sock);
static void cmd_launch(char * cmd, char * cmd_type);
/*
 * Parses a single field and returns it.
 */
static char * parse_single(char * cmd, char ** save);

void cmd_handle(char * cmd)
{
	char * save = cmd;

	char * cmd_type = parse_single(cmd, &save);

	//Convert to low chars
	char * cmd_low = strlwr(cmd_type);

	cmd_launch(save, cmd_low);
}

bool cmd_send_dicover_message(int sender, int src_node, int dst_node, int * connfd, int * minimum_path, int * path)
{
	char buff[HEADER_SIZE + PAYLOAD_SIZE];
	char respond[HEADER_SIZE + PAYLOAD_SIZE];
	message_t msg = {.msg_id = -1, .src_id = src_node,
			.dst_id = -1, .trailing = 0, .func_id = DISCOVER};//Place holder

	memset(msg.payload, 0, sizeof(msg.payload));
	snprintf((char *)msg.payload, sizeof(dst_node), "%d", dst_node);//Copy to payload the destination node.
	size_t min_path = AMOUNT_OF_NODES;
	//Table for all possible route from source to destination.
	int routes[AMOUNT_OF_NODES + 1][AMOUNT_OF_NODES + 1];
	int connections[AMOUNT_OF_NODES];
	memset(connections, -1, sizeof(int) * AMOUNT_OF_NODES);//Initialize connection array to -1.

	for(int i = 0; i < AMOUNT_OF_NODES; i++)
	{
		bool helpful = false;
		if(node_is_neighbors(src_node, i) && sender != i && src_node != i)
		{
			msg.msg_id = protocol_get_msg_id();
			msg.dst_id = i;
			//Build up the message.
			protocol_create_message(buff, sizeof(buff), &msg);
			netowrk_set_timeout(network_get_timeout()/2);//Cut the timeout by half.
			int sock;
			int size = netowrk_send_to_node(msg.dst_id, buff, sizeof(buff), &sock);
			connections[i] = sock;
			if(size <= 0)
			{
				printf("send_to_node() failed");
				continue;
			}
			if(network_recv_from_node(sock, respond, sizeof(respond)) <= 0)
			{
				//Legitimate fail, if node is waiting for respond
				continue;
				netowrk_set_timeout(network_get_timeout() * 2);//set the time out.
			}

			message_t msg_respond;
			memcpy(&msg_respond, respond, sizeof(msg_respond));

			if(msg_respond.src_id == i && msg_respond.func_id == ROUTE)
			{
				helpful = true;//Means the node has helpful information.
				char strp[sizeof(size_t) + 1];//+1 for NULL terminator.
				memcpy(strp, msg_respond.payload + sizeof(size_t), sizeof(size_t));
				size_t amount_nodes = atoi(strp);

				int j = 1;
				//Means we have to add ourself as node in the path.
				if(msg_respond.dst_id != cli_get_context())
				{
					routes[i][j++] = msg_respond.dst_id;
					amount_nodes++;
				}
				//Update minimum path to the path with minimum nodes in it's way.
				min_path = amount_nodes < min_path ? amount_nodes : min_path;
				routes[i][0] = amount_nodes;

				for(int payload_ind = 2; j <= amount_nodes; j++, payload_ind++)//<= since we starting from index 1
				{
					//+2 since the actual nodes number starting from byte 8
					memcpy(strp, msg_respond.payload + (sizeof(size_t) *(payload_ind)), sizeof(size_t));
					int node_id = atoi(strp);
					routes[i][j] = node_id;//Update routes table.
				}
			}
		}

		if(!helpful)
		{
			//Mark as a node which has no route for destination.
			routes[i][0] = -1;
		}
	}

	netowrk_set_timeout(TIMEOUT);//Reset the time out.
	//Now scan the routes table and find the minimum path(and lowest index)
	int route_ind = AMOUNT_OF_NODES;

	for(int i = 0; i < AMOUNT_OF_NODES; i++)
	{
		if(routes[i][0] == min_path)
		{
			//We have found the minimum ind.
			route_ind = i;
			break;
		}
	}

	//Close all open connection except the one we need
	for(int i = 0; i < AMOUNT_OF_NODES; i++)
	{
		if(route_ind != i && connections[i] != -1)
		{
			network_close_connection_node(connections[i]);
		}
	}

	if(connfd != NULL && route_ind != AMOUNT_OF_NODES)
	{
		*connfd = connections[route_ind];
	}
	if(minimum_path != NULL && route_ind != AMOUNT_OF_NODES)
	{
		*minimum_path = min_path;
	}
	if(path != NULL && route_ind != AMOUNT_OF_NODES)
	{
		for(int i = 0; i < AMOUNT_OF_NODES; i++)
		{
			path[i] = routes[route_ind][i];
		}
	}

	return route_ind != AMOUNT_OF_NODES;//Returns true if path exists.
}

static int set_id(char * cmd)
{
	int retval = 0;

	const size_t amount = 1;//Amount of parameters to parse.
	char params[amount][MAX_MSG_SIZE];//output parameter.

	if(cmd_parse(cmd, params, amount) > 0)
	{
		int dst_node = atoi(params[0]);
		if(util_validate_node_id(dst_node))//Validate the node
		{
			char buff[HEADER_SIZE + PAYLOAD_SIZE];
			char respond[HEADER_SIZE + PAYLOAD_SIZE];

			message_t msg = {.msg_id = protocol_get_msg_id(), .src_id = cli_get_context(),
					.dst_id = dst_node, .trailing = 0, .func_id = SETID};

			memset(msg.payload, 0, sizeof(msg.payload));
			//Build up the message.
			snprintf((char *)msg.payload, sizeof(dst_node), "%d", dst_node);
			protocol_create_message(buff, sizeof(buff), &msg);
			int sock;
			if(netowrk_send_to_node(dst_node, buff, sizeof(buff), &sock) <= 0)
			{
				printf("send_to_node() failed\n");
				retval = -1;
			}
			else if(network_recv_from_node(sock, respond, sizeof(respond)) <= 0)
			{
				printf("recv_from_node() failed\n");
				retval = -1;
			}
			else
			{
				message_t msg_respond;
				memcpy(&msg_respond, respond, sizeof(msg));
				if(msg_respond.func_id == ACK)
				{
					//Node output
					printf("%s\n", MSG_ACK);
					//Set CLI context to the current node.
					cli_set_context(dst_node);
				}
				else if(msg_respond.func_id == NACK)
				{
					printf("%s\n", MSG_NACK);
				}
			}

			//Close the connection
			network_close_connection_node(sock);
		}
		else
		{
			printf("Node %d does not exists\n", dst_node);
		}
	}

	return retval;
}

static int connect(char * cmd)
{
	const size_t amount = 2;//Amount of parameters to parse.
	char params[amount][MAX_MSG_SIZE];//output parameter.

	if(cmd_parse(cmd, params, amount) > 0)
	{
		char * ip = params[0];//IP
		unsigned short port =  atoi(params[1]);//Second parameter should be port
		int dst_node = network_get_node_id(port);//translate node_id by port.

		if(dst_node == cli_get_context())
		{
			printf("Can't connect a node to himself\n");
			return 0;
		}
		if(strcmp(ip, IP_ADDRESS))
		{
			printf("Target IP address isn't part of the network\n");
			return 0;
		}
		else if(dst_node == -1)
		{
			printf("port %d isn't part of the network\n", port);
			return 0;
		}

		//Done checking, now creating the message.
		char buff[HEADER_SIZE + PAYLOAD_SIZE];
		char respond[HEADER_SIZE + PAYLOAD_SIZE];
		message_t msg = {.msg_id = protocol_get_msg_id(), .src_id = cli_get_context(),
				.dst_id = dst_node, .trailing = 0, .func_id = CONNECT};
		memset(msg.payload, 0, sizeof(msg.payload));
		//Build up the message.
		protocol_create_message(buff, sizeof(buff), &msg);
		int sock;
		if(netowrk_send_to_node(dst_node, buff, sizeof(buff), &sock) <= 0)
		{
			printf("send_to_node() failed\n");
			return -1;
		}
		else if(network_recv_from_node(sock, respond, sizeof(respond)) <= 0)
		{
			printf("recv_from_node() failed\n");
			return -1;
		}
		message_t msg_respond;
		memcpy(&msg_respond, respond, sizeof(msg_respond));

		char str_respond[MAX_RESPOND_SIZE] = MSG_ACK;
		if(msg_respond.func_id == NACK)
		{
			strncpy(str_respond, MSG_NACK, sizeof(str_respond));
		}

		printf("%s\n%s\n", str_respond, msg_respond.payload);
		//Close the socket
		network_close_connection_node(sock);
	}

	return 0;

}

static int send(char * cmd)
{
	const size_t amount = 3;//Amount of parameters to parse.
	char params[amount][MAX_MSG_SIZE];//output parameter.

	if(cmd_parse(cmd, params, amount) > 0)
	{
		int dst_node = atoi(params[0]);
		size_t msg_size = atoi(params[1]);
		char * payload = params[2];//The actual message.

		if(!(util_validate_node_id(dst_node)))
		{
			printf("Node %d doesn't exists\n", dst_node);
			return 0;
		}
		if(node_is_neighbors(cli_get_context(), dst_node))
		{
			if(cmd_send_inner(dst_node, msg_size, payload, -1) < 0)
			{
				printf("cmd_send_inner() failed\n");
				return -1;
			}
		}
		else//Nodes are not connected, need to send discover.
		{
			if(send_discover(dst_node, payload, msg_size) == -1)
			{
				/*
				 * Means we can't send the message to the destination node since
				 * there is no route..
				 */
				printf("%s\n", MSG_NACK);
			}
		}
	}

	return 0;
}

static int route(char * cmd)
{
	const size_t amount = 1;//Amount of parameters to parse.
	char params[amount][MAX_MSG_SIZE];//output parameter.
	int routes[AMOUNT_OF_NODES];
	int min_path = 1;
	bool found_route = true;

	if(cmd_parse(cmd, params, amount) > 0)
	{
		int dst_node = atoi(params[0]);
		if(!util_validate_node_id(dst_node))//Validate the node
		{
			printf("Node %d doesn't exists\n", dst_node);
			return 0;
		}
		else if(dst_node == cli_get_context())
		{
			printf("%s\n%d\n", MSG_ACK, dst_node);
			return 0;
		}
		if(node_is_neighbors(cli_get_context(), dst_node))
		{
			routes[0] = min_path;
			routes[1] = dst_node;
		}
		else//Nodes are not connected, need to send route message to the current node(and then he will send discover).
		{
			char buff[HEADER_SIZE + PAYLOAD_SIZE];
			char respond[HEADER_SIZE + PAYLOAD_SIZE];
			message_t msg = {.msg_id = protocol_get_msg_id(), .src_id = cli_get_context(),
					.dst_id = cli_get_context(), .trailing = 0, .func_id = ROUTE};

			memset(msg.payload, 0, sizeof(msg.payload));
			snprintf((char *)msg.payload, sizeof(dst_node), "%d", dst_node);//Copy to payload the destination node.
			protocol_create_message(buff, sizeof(buff), &msg);
			int sock;

			if(netowrk_send_to_node(msg.dst_id, buff, sizeof(buff), &sock) <= 0)
			{
				printf("send_to_node() failed\n");
				return -1;
			}
			else if(network_recv_from_node(sock, respond, sizeof(respond)) <= 0)
			{
				printf("recv_from_node() failed.node: %d\n", cli_get_context());
				return -1;
			}
			else
			{
				message_t msg_respond;
				memcpy(&msg_respond, respond, sizeof(msg));
				if(msg_respond.func_id == ACK)
				{
					//Copy the answer into routes.
					memcpy(routes, msg_respond.payload + sizeof(msg.msg_id), sizeof(routes));
				}
				else if(msg_respond.func_id == NACK)
				{
					printf("No route found\n");
					found_route = false;
				}
			}

			//Close the connection
			network_close_connection_node(sock);
			netowrk_set_timeout(TIMEOUT);
		}

		if(found_route)
		{
			printf("%s\n", MSG_ACK);
			printf("%d->", cli_get_context());
			for(int i = 0; i <routes[0]; i++)
			{
				(i + 1 == routes[0]) ? printf("%d\n", routes[i + 1]) : printf("%d->", routes[i + 1]);
			}
		}
	}

	return 0;
}

static int peers(char * cmd)
{
	int dst_node = cli_get_context();
	char buff[HEADER_SIZE + PAYLOAD_SIZE];
	char respond[HEADER_SIZE + PAYLOAD_SIZE];
	message_t msg = {.msg_id = protocol_get_msg_id(), .src_id = cli_get_context(),
			.dst_id = dst_node, .trailing = 0, .func_id = PEERS};

	memset(msg.payload, 0, sizeof(msg.payload));
	//Build up the message.
	protocol_create_message(buff, sizeof(buff), &msg);
	int sock;
	if(netowrk_send_to_node(dst_node, buff, sizeof(buff), &sock) <= 0)
	{
		printf("send_to_node() failed");
		return -1;
	}
	else if(network_recv_from_node(sock, respond, sizeof(respond)) <= 0)
	{
		printf("recv_from_node() failed\n");
		return -1;
	}

	message_t msg_respond;
	memcpy(&msg_respond, respond, sizeof(msg_respond));
	char str_respond[MAX_RESPOND_SIZE] = MSG_ACK;
	if(msg_respond.func_id == NACK)
	{
		strncpy(str_respond, MSG_NACK, sizeof(str_respond));
		printf("%s\n", str_respond);
	}
	else
	{
		printf("%s\n", str_respond);
		node_print_neighbors(dst_node);
	}

	network_close_connection_node(sock);

	return 0;
}

static int cmd_parse(char * cmd , char params[][MAX_MSG_SIZE], int amount)
{
	char * save = cmd;
	int parsed = 0;

	//Parsing the command.
#ifdef _WIN32
	char * tok = strtok_s(cmd, ", :", &save);
#else
	char * tok = strtok_r(cmd, ", :", &save);
#endif //_WIN32
	while(tok && amount > 0)
	{
		//Deep copy. copies TOKEN_MAX_SIZE or until NULL terminator. what comes first.
		strncpy(params[parsed], tok, MAX_MSG_SIZE);
		amount--;
		parsed++;

#ifdef _WIN32
		tok = (char *)strtok_s(NULL, ",", &save);
#else
		tok = (char *)strtok_r(NULL, ",", &save);
#endif //_WIN32
	}

	return parsed;
}

static int send_discover(int dst_node, char * message, size_t msg_size)
{
	int sock;
	int min_path;
	int routes[AMOUNT_OF_NODES];

	if(!cmd_send_dicover_message(cli_get_context(), cli_get_context(), dst_node, &sock, &min_path, routes))
	{
		//No route for destination
		return -1;
	}
	network_close_connection_node(sock);
	if(relay(routes, min_path - 1, dst_node, &sock) < 0)
	{
		printf("relay() failed\n");
		return -1;
	}//After finish sending all relays, create and send a "Send" message.
	else if(cmd_send_inner(dst_node, msg_size, message, sock) < 0)
	{
		printf("cmd_send_inner() failed\n");
		return -1;
	}
	//Close the connection to the node we needed.
	network_close_connection_node(sock);
	netowrk_set_timeout(TIMEOUT);

	return 0;
}

static int relay(int routes[AMOUNT_OF_NODES], uint32_t num_of_relays, int dst_node, int * connfd)
{
	/*
	 * Send all relay messages to the destination. Starting from 1
	 * since routes[route_ind][0] contains amount of nodes.
	 */
	char buff[HEADER_SIZE + PAYLOAD_SIZE];
	char respond[HEADER_SIZE + PAYLOAD_SIZE];
	int sock = -1;
	//Create Relay message
	message_t relay_msg = {.msg_id = -1, .src_id = cli_get_context(),
			.dst_id = -1, .trailing = num_of_relays, .func_id = RELAY};
	for(int i = 1; i < routes[0]; i++)
	{
		relay_msg.msg_id = protocol_get_msg_id();
		relay_msg.dst_id = routes[i];
		snprintf((char *)relay_msg.payload, sizeof(dst_node), "%d", routes[i + 1]);//Copy to payload the next node.
		snprintf((char *)(relay_msg.payload + sizeof(dst_node)), sizeof(dst_node), "%d", num_of_relays--);//Copy the number of messages to relay.
		protocol_create_message(buff, sizeof(relay_msg), &relay_msg);
		int send;
		if(sock == -1)
		{
			send = netowrk_send_to_node(relay_msg.dst_id, buff, sizeof(buff), &sock);
		}
		else
		{
			send = network_send_open_connection(sock, buff, sizeof(buff));
		}
		if(send <= 0)
		{
			printf("send/send_open_connection():RELAY message failed\n");
			return -1;
		}
		if(network_recv_from_node(sock, respond, sizeof(respond)) <= 0)
		{
			printf("recv_from_node() failed\n");
			return -1;
		}

		message_t msg_respond;
		memcpy(&msg_respond, respond, sizeof(msg_respond));

		if(msg_respond.func_id != ACK || msg_respond.src_id != routes[1] || msg_respond.dst_id != cli_get_context())
		{
			printf("Data corruption. func_id:%d, src_id:%d, dst_id:%d\n", msg_respond.func_id, msg_respond.src_id, msg_respond.dst_id);
			return -1;
		}
	}

	//Save the socket of the connection
	if(connfd != NULL)
	{
		*connfd = sock;
	}

	return 0;
}

static int cmd_send_inner(int dst_node, size_t msg_size, char * payload, int open_sock)
{
	char buff[HEADER_SIZE + PAYLOAD_SIZE];
	char respond[HEADER_SIZE + PAYLOAD_SIZE];

	//Create the message
	message_t msg = {.msg_id = protocol_get_msg_id(), .src_id = cli_get_context(),
			.dst_id = dst_node, .trailing = (msg_size-1)/(MAX_PAYLOAD_SIZE), .func_id = SEND};

	memset(msg.payload, 0, sizeof(msg.payload));
	snprintf((char *)msg.payload, sizeof(msg_size), "%u", msg_size);//Copy size of the message(automatic convert into ASCII)
	memcpy(msg.payload + sizeof(msg_size), payload, MAX_PAYLOAD_SIZE);//Copy the message.
	//Update msg_size.
	msg_size =  msg_size <= (MAX_PAYLOAD_SIZE) ? 0 : msg_size - (MAX_PAYLOAD_SIZE);
	//Build up the message.
	protocol_create_message(buff, sizeof(buff), &msg);
	int sock;
	int size;
	//Since open_sock can be an open connection
	if(open_sock != -1)
	{
		//Assign to sock
		sock = open_sock;
		size = network_send_open_connection(sock, buff, sizeof(buff));
	}
	else
	{
		size = netowrk_send_to_node(dst_node, buff, sizeof(buff), &sock);
	}

	if(size <= 0)
	{
		printf("send_to_node() failed\n");
		return -1;
	}
	else if(network_recv_from_node(sock, respond, sizeof(respond)) <= 0)
	{
		printf("recv_from_node() failed\n");
		return -1;
	}

	//Read respond
	message_t msg_respond;
	memcpy(&msg_respond, respond, sizeof(msg_respond));
	char str_respond[MAX_RESPOND_SIZE] = MSG_ACK;
	if(msg_respond.func_id == NACK)
	{
		strncpy(str_respond, MSG_NACK, sizeof(str_respond));
	}

	//If message is bigger then MAXIMUM_PAYLOAD_SIZE, we sending in peaces
	int ind = MAX_PAYLOAD_SIZE;//Index for the payload.
	while(msg_size > 0)
	{
		/*
		 * Second trailing message doesn't contains the message size so they
		 * can contain maximum of PAYLOAD_SIZE.
		 */
		msg.msg_id = protocol_get_msg_id();
		//Adding minus one since we have edge case when msg_size equals PAYLOAD_SIZE
		msg.trailing = (msg_size-1)/PAYLOAD_SIZE;
		memset(msg.payload, 0, sizeof(msg.payload));
		memcpy((char *)(msg.payload),  payload + ind, PAYLOAD_SIZE);//Copy the message.
		//Update msg_size.
		msg_size =  msg_size <= (PAYLOAD_SIZE) ? 0 : msg_size - PAYLOAD_SIZE;
		//If there is another part to the message, it means we read PAYLOAD_SIZE. Otherwise we won't have another iteration.
		ind += PAYLOAD_SIZE;
		//Build up the message.
		protocol_create_message(buff, sizeof(buff), &msg);
		if(network_send_open_connection(sock, buff, sizeof(buff)) < 0)
		{
			printf("send_open_connection() failed\n");
			return -1;
		}
		else if(network_recv_from_node(sock, respond, sizeof(respond)) < 0)
		{
			printf("recv_from_node() failed\n");
			return -1;
		}

		memcpy(&msg_respond, respond, sizeof(msg_respond));
		if(msg_respond.func_id != ACK || msg_respond.src_id != dst_node ||msg_respond.dst_id != cli_get_context())
		{
			//Something happened. breaking the loop.
			strncpy(str_respond, MSG_NACK, sizeof(str_respond));
			printf("Data corruption. func_id:%d, src_id:%d, dst_id:%d\n", msg_respond.func_id, msg_respond.src_id, msg_respond.dst_id);
			break;
		}
	}

	printf("%s\n", str_respond);
	if(open_sock == -1)
	{
		network_close_connection_node(sock);
	}

	return 0;
}

static void cmd_launch(char * cmd, char * cmd_type)
{
	if(cmd == NULL || cmd_type == NULL)
	{
		return;
	}
	if(!strncmp(cmd_type, g_setid, CMD_TYPE_MAX_SIZE))
	{
		set_id(cmd);
	}
	else if(!strncmp(cmd_type, g_connect, CMD_TYPE_MAX_SIZE))
	{
		connect(cmd);
	}
	else if(!strncmp(cmd_type, g_send, CMD_TYPE_MAX_SIZE))
	{
		send(cmd);
	}
	else if(!strncmp(cmd_type, g_route, CMD_TYPE_MAX_SIZE))
	{
		route(cmd);
	}
	else if(!strncmp(cmd_type, g_peers, CMD_TYPE_MAX_SIZE))
	{
		peers(cmd);
	}
	else
	{
		printf("\'%s\' is not recognized as an internal or external command.\n", cmd_type);
	}
}

static char * parse_single(char * cmd, char ** save)
{
#ifdef _WIN32
	return (char *)strtok_s(cmd, ", :", save);
#else
	return (char *)strtok_r(cmd, ", :", save);
#endif //_WIN32
}
