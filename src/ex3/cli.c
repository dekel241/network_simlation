/*
 * cli.c
 *
 *  Created on: Jan 7, 2019
 *      Author: Dekel
 */
#include "cli.h"
#include "select.h"
#include "protocol.h"
#include "cmd.h"
#include "config.h"
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>

/*
 * The current node to send commands from it. 65536 means no context.
 * When application starts, no context defined.
 */
static int g_context = NO_CONTEXT;

void cli_begin()
{
	char buff[MAX_MSG_SIZE];
	memset(buff, 0, sizeof(buff));
	sleep(0.5);//Waiting for half a second.
	while(true)
	{
		printf(">");
		fflush(stdout);
#ifdef _WIN32
		buff[0] = getchar();//Clearing the stdin
#endif
		int sock = wait_for_input();
		if(sock != -1)
		{
			int size = read(sock, buff, sizeof(buff));
			if(size > 0)
			{
				//Means it is a user input
				if(sock == STDIN_FILENO && buff[1] != 0)
				{
					//Get rid of the line feed.
					buff[strcspn(buff, "\n")] = 0;
					cmd_handle(buff);
				}
			}
		}
		else
		{
			//Read from standard input
			fgets(buff + 1, sizeof(buff), stdin);
			if(buff[1] != 0)
			{
				//Get rid of the line feed.
				buff[strcspn(buff, "\n")] = 0;
				//Handle the command
				cmd_handle(buff);
			}
		}

		memset(buff, 0, sizeof(buff));
	}
}

void cli_set_context(int node_id)
{
	g_context = node_id;
}

int cli_get_context(void)
{
	return g_context;
}
