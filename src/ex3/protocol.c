/*
 * protocol.c
 *
 *  Created on: Jan 8, 2019
 *      Author: Dekel
 */
#include "protocol.h"
#include "config.h"
#include "select.h"
#include "node.h"
#include "util.h"

#ifdef _WIN32
#include <winsock.h>
#include <io.h>
#else //Linux
	#include <netinet/ip.h>
	#include <netinet/in.h>
	#include <unistd.h>
	#include <arpa/inet.h>
#endif //_WIN32
#include <assert.h>
#include <stdio.h>
#include <memory.h>

static node_t g_nodes[AMOUNT_OF_NODES];//Contains file descriptors for each node.
static int g_msg_id = 0;

int protocol_global_init(void)
{
	for(int i = 0; i < AMOUNT_OF_NODES; i++)
	{
		int sock = socket(AF_INET, SOCK_STREAM, 0);
		 if (sock == -1)
		 {
			printf("socket creation failed...\n");
			return -1;
		}
		 struct timeval timeout = {.tv_sec = 5000, .tv_usec = 0};//5 seconds
		 //Set some socket options
		setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char *)&timeout, sizeof(timeout));
		setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (const char *)&timeout, sizeof(timeout));

		g_nodes[i].sock = sock;
		g_nodes[i].id = i;
		//Add to monitoring
		add_fd_to_monitoring(sock);//Stdin already inside
	}

	return 0;
}

void protocol_global_destroy(void)
{
	for(int i = 0; i < AMOUNT_OF_NODES; i++)
	{
		close(g_nodes[i].sock);
	}
}

void protocol_create_message(char * buff, size_t size, message_t * msg)
{
	assert(size == HEADER_SIZE + PAYLOAD_SIZE);

	memcpy(buff, msg, size);
}

node_t * protocol_get_node(int node_id)
{
	node_t * retval = NULL;
	if(util_validate_node_id(node_id))
	{
		retval = &g_nodes[node_id];
	}

	return retval;
}

int protocol_get_msg_id(void)
{
	int retval = g_msg_id++;
	return retval;
}
